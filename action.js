let boardArea = {};
let board = {};
let lock = false;

const saidas = document.getElementById('outputArea');
let cell = new Array(7); // declara e define cell como um array de 7 elementos (para as 7 colunas).
let colu = new Array(7);
let control = new Array(7);

const geraTabuleiro = function() {
    boardArea = document.getElementById('tabuleiro');
    board = document.createElement('div');
    boardArea.appendChild(board);
    board.id = 'boardGame';

    for (let i = 0; i < cell.length; i++) {
        cell[i] = new Array(6); // faz com que cada elemento seja um array de 6 elementos, definindo cell como um array bidimensional.
        control[i] = new Array(6); // este é o array gêmeo ao cell que vai segurar valores booleanos, para ajudar na logica de controle.
        colu[i] = document.createElement('div');
        board.appendChild(colu[i]);
        colu[i].id = `coluna${i}`;
        colu[i].className = 'colunas';
        for (let j = 0; j < cell[i].length; j++) {
            cell[i][j] = document.createElement('div');
            control[i][j] = false; // coloca todos os espaços do array de cotrole como falsos, representando celulas vazias.
            colu[i].appendChild(cell[i][j]);
            cell[i][j].className = `celulaVazia`;
            cell[i][j].id = `celula${i}${j}`;
        }
    }
}
geraTabuleiro();

// essa variavel armazena a quantidade de clicks pra saber qual disco será colocado;
let contador = 0;
let timer = 200;
const turns = document.createElement('div');
    saidas.appendChild(turns);
    turns.innerHTML = 'T<br>U<br>R<br>N';
    turns.id = 'turnsCounter';
const jogada = document.createElement('div');
    saidas.appendChild(jogada);
    jogada.innerText = contador;
    jogada.id = 'turns';
const vez = document.createElement('div');
    saidas.appendChild(vez);
    vez.innerText = "Player 1";
    vez.id = 'playersTurns';
    vez.className = 'player1';
const placar = document.createElement('div');
    saidas.appendChild(placar);
    placar.id = 'score';
const letrasPlacar = document.createElement('div');
    placar.appendChild(letrasPlacar);
    letrasPlacar.innerHTML = 'S<br>C<br>O<br>R<br>E';
    letrasPlacar.id = 'scoreLetters';
let scoreP1 = 0;
let scoreP2 = 0;
const placarP1 = document.createElement('div');
    placar.appendChild(placarP1);
    placarP1.innerText = scoreP1;
    placarP1.id = 'scoreNumber';
    placarP1.className = 'player1';
const placarP2 = document.createElement('div');
    placar.appendChild(placarP2);
    placarP2.innerText = scoreP2;
    placarP2.id = 'scoreNumber';
    placarP2.className = 'player2';

    //Variaveis de efeitos sonoros
let coinEffect = new Audio('./msc/coinInsert.mp3') 
let victorySound = new Audio('./msc/epicVictory.mp3')
let efeitoEmpate = new Audio('./msc/lose.mp3')
// let coluna0 = document.getElementById('coluna0') -----> substituido por colu[0], por exemplo.
const geraZonasClicaveis = function() {
    for (let i = 0; i < colu.length; i++) {
        colu[i].addEventListener('click', function () {
            if(!lock) {
                let disco = document.createElement('div')
                if (contador % 2 === 0) {
                    disco.id = 'disco1';
                    for (let j = 5; j >= 0; j--) { // o loop pega o teto de j, que representa a última linha de baixo, e vai decrescendo.
                        if (control[i][j] === false) { // assim, ele verifica se esse lugar está vago, no aray gêmeo booleano de cell.
                            contador = contador + 1;
                            coinEffect.play()
                            cell[i][j].appendChild(disco); // implanta o disco dentro da célula respectiva.
                            control[i][j] = true; // acusa sua ocupação no gemeo, espelhando-o.
                            cell[i][j].className = 'celulaPrenchida1';
                            disco.className = 'slideDown';
                            window.setTimeout(() => { disco.classList.remove('slideDown') }, timer);
                            vez.innerText = "Player 2";
                            vez.className = 'player2'
                            break; // e isso aqui impede da coluna inteira ser preenchida num só clique.
                        }
                    }
                    // colu[i].lastChild.appendChild(disco) <---- substituido pelo loop acima.
                }
                else {
                    disco.id = 'disco2';
                    for (let j = 5; j >= 0; j--) { // identico ao de cima, o que significa que talvez a gente pudesse sepparar isso aqui dentro de uma função mais tarde.
                        if (control[i][j] === false) {
                            contador = contador + 1;
                            coinEffect.play()
                            cell[i][j].appendChild(disco);
                            control[i][j] = true;
                            cell[i][j].className = 'celulaPrenchida2'
                            disco.className = 'slideDown';
                            window.setTimeout(() => { disco.classList.remove('slideDown') }, timer);
                            vez.innerText = "Player 1";
                            vez.className = 'player1'
                            break;
                        }
                    }
                    // colu[i].lastChild.appendChild(disco)
                }
                jogada.innerText = contador;

                let win = verificaVitoria()

                if(win) {
                    // desenha tela de vitória
                    lock = true;
                    vez.innerText = "WON!!!";
                    window.setTimeout(gameOverV, 1500);
                }
                else if(contador === 42) {
                    empate();
                }
            }
        })
    }
}
geraZonasClicaveis();

const buttonEffect = function() {
    boardArea = document.getElementById('tabuleiro');
    final = document.getElementById('resetScreen');
    boardArea.removeChild(final);
    geraTabuleiro();
    geraZonasClicaveis();
    vez.innerText = "Player 1";
    contador = 0;
    jogada.innerText = contador;
    lock = false;
}

const geraTelaFinal = function() {
    boardArea.removeChild(board);
    let final = document.createElement('div');
        boardArea.appendChild(final);
        final.id = 'resetScreen';
    let messageGO = document.createElement('p');
        final.appendChild(messageGO);
        messageGO.innerText = 'Game Over';
        messageGO.id = 'mensagemGO';
}

const gameOverV = function() {
    geraTelaFinal();
    let jogadorVitorioso = document.createElement('p');
    let final = document.getElementById('resetScreen');
        final.appendChild(jogadorVitorioso);
    if(contador % 2 === 1) {
        // player 1 venceu
        scoreP1++;
        placarP1.innerText = scoreP1;
        jogadorVitorioso.innerText = 'Player 1 won!';
        jogadorVitorioso.id = 'player1won';
        victorySound.play()
    }
    else {
        // player 2 venceu
        scoreP2++;
        placarP2.innerText = scoreP2;
        jogadorVitorioso.innerText = 'Player 2 won!'
        jogadorVitorioso.id = 'player2won';
        victorySound.play()
    }
    let respect = document.createElement('p');
        final.appendChild(respect);
        respect.innerText = 'respect +';
        respect.id = 'respeito';
    let button = document.createElement('button');
        final.appendChild(button);
        button.id = 'restartButton';
        button.innerText = 'restart';
        button.addEventListener('click', buttonEffect);
}

const empate = function() {
    geraTelaFinal();
    let draw = document.createElement('p');
    let final = document.getElementById('resetScreen');
        efeitoEmpate.play()
        final.appendChild(draw);
        draw.innerText = 'DRAW...';
        draw.id = 'drawMessage';
    let button = document.createElement('button');
        final.appendChild(button);
        button.id = 'restartButton';
        button.innerText = 'restart';
        button.addEventListener('click', buttonEffect);
}

function verificaVitoria () {

    let resposta = false;
    const edgeX = cell.length - 3;
    const edgeY = cell[0].length - 3;

    for (let x = 0; x < edgeX; x++) {
        for (let y = 0; y < cell[x].length; y++) {
            if (cell[x][y].className !== 'celulaVazia') {
                if (cell[x][y].className === cell[x + 1][y].className && 
                    cell[x][y].className === cell[x + 2][y].className && 
                    cell[x][y].className === cell[x + 3][y].className) {
                    resposta = true;
                    cell[x][y].className = 'celulaVitoria';
                    cell[x+1][y].className = 'celulaVitoria';
                    cell[x+2][y].className = 'celulaVitoria';
                    cell[x+3][y].className = 'celulaVitoria';
                    break;
                }
            }
        }
    }

    for (let x = 0; x < cell.length; x++) {
        for (let y = 0; y < edgeY; y++) {
            if (document.getElementById(`celula${x}${y}`).className !== 'celulaVazia') {
                if (document.getElementById(`celula${x}${y}`).className === 
                    document.getElementById(`celula${x}${y+1}`).className && 
                    document.getElementById(`celula${x}${y}`).className === 
                    document.getElementById(`celula${x}${y+2}`).className &&
                    document.getElementById(`celula${x}${y}`).className === 
                    document.getElementById(`celula${x}${y+3}`).className) {
                    resposta = true;
                    document.getElementById(`celula${x}${y}`).className = 'celulaVitoria';
                    document.getElementById(`celula${x}${y+1}`).className = 'celulaVitoria';
                    document.getElementById(`celula${x}${y+2}`).className = 'celulaVitoria';
                    document.getElementById(`celula${x}${y+3}`).className = 'celulaVitoria';
                    break;
                }
            }
        }
    }

    for (let x = 0; x < edgeX; x++) {
        for (let y = 0; y < edgeY; y++) {
            if (document.getElementById(`celula${x}${y}`).className !== 'celulaVazia') {
                if (document.getElementById(`celula${x}${y}`).className === 
                    document.getElementById(`celula${x+1}${y+1}`).className && 
                    document.getElementById(`celula${x}${y}`).className === 
                    document.getElementById(`celula${x+2}${y+2}`).className &&
                    document.getElementById(`celula${x}${y}`).className === 
                    document.getElementById(`celula${x+3}${y+3}`).className) {
                    resposta = true;
                    document.getElementById(`celula${x}${y}`).className = 'celulaVitoria';
                    document.getElementById(`celula${x+1}${y+1}`).className = 'celulaVitoria';
                    document.getElementById(`celula${x+2}${y+2}`).className = 'celulaVitoria';
                    document.getElementById(`celula${x+3}${y+3}`).className = 'celulaVitoria';
                    break;
                }
            }
        }
    }

    for (let x = 3; x < cell.length; x++) {
        for (let y = 0; y < edgeY; y++) {
            if (document.getElementById(`celula${x}${y}`).className !== 'celulaVazia') {
                if (document.getElementById(`celula${x}${y}`).className === 
                    document.getElementById(`celula${x-1}${y+1}`).className && 
                    document.getElementById(`celula${x}${y}`).className === 
                    document.getElementById(`celula${x-2}${y+2}`).className &&
                    document.getElementById(`celula${x}${y}`).className === 
                    document.getElementById(`celula${x-3}${y+3}`).className) {
                    resposta = true;
                    document.getElementById(`celula${x}${y}`).className = 'celulaVitoria';
                    document.getElementById(`celula${x-1}${y+1}`).className = 'celulaVitoria';
                    document.getElementById(`celula${x-2}${y+2}`).className = 'celulaVitoria';
                    document.getElementById(`celula${x-3}${y+3}`).className = 'celulaVitoria';
                    break;
                }
            }
        }
    }
    
    return resposta;
}
